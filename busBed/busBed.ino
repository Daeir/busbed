#include <Adafruit_NeoPixel.h>
#include <Servo.h>
#include <DFRobotDFPlayerMini.h>
//#include "Button.h"

//pin definitions
const int koplampLinksPin = 50;
const int koplampRechtsPin = 52;
const int knipperlichtLinksPin = 48;
const int knipperlichtRechtsPin = 44;
const int cabinelichtPin = 42;
const int zwaailichtPin = 40;
const int feestlichtPin = 38;
const int snelheidsmeterPin = 46;
const int snelheidslichtPin = 36;
const int gearPin[7] = {2,3,4,5,6,7,16};
const int toeterPin = 17;
const int koplampKnop = 22;
const int knipperLinksKnop = 24;
const int knipperRechtsKnop = 26;
const int sirenestandKnop = 28;
const int discostandKnop = 30;
const int feestlichtKnop = 32;
const int cabineKleurPin = A0;
const int cabineIntensiteitPin = A1;
const int schakellichtPin = 34;

//creating objects
Adafruit_NeoPixel koplampLinks(7, koplampLinksPin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel koplampRechts(7, koplampRechtsPin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel knipperlichtLinks(1, knipperlichtLinksPin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel knipperlichtRechts(1, knipperlichtRechtsPin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel cabinelicht(45, cabinelichtPin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel snelheidslicht(7, snelheidslichtPin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel schakellicht(8, schakellichtPin, NEO_GRB + NEO_KHZ800);
Servo snelheidsmeter;
DFRobotDFPlayerMini myDFPlayer;

//knipperlicht variables

void setup() {

	//serial for debugging 
	Serial.begin(115200);
	Serial3.begin(9600);

	snelheidsmeter.attach(snelheidsmeterPin);

	pinMode(zwaailichtPin, OUTPUT);
	pinMode(feestlichtPin, OUTPUT);

	pinMode(toeterPin, INPUT);
	pinMode(koplampKnop, INPUT);
	pinMode(knipperLinksKnop, INPUT);
	pinMode(knipperRechtsKnop, INPUT);
	pinMode(sirenestandKnop, INPUT);
	pinMode(discostandKnop, INPUT);
	pinMode(feestlichtKnop, INPUT);
	for(int i=0; i<7; i++){
	    
	    pinMode(gearPin[i], INPUT);
	}

	myDFPlayer.begin(Serial3);
	myDFPlayer.volume(30);
	myDFPlayer.play(1);

	//turn all pixels to black
	koplampLinks.begin();
	koplampLinks.show();
	koplampRechts.begin();
	koplampRechts.show();
	knipperlichtLinks.begin();
	knipperlichtLinks.show();
	knipperlichtRechts.begin();
	knipperlichtRechts.show();
	cabinelicht.begin();
	cabinelicht.show();
	snelheidslicht.begin();
	snelheidslicht.show();
	schakellicht.begin();
	schakellicht.show();

	snelheidsmeter.write(0);
	digitalWrite(zwaailichtPin, HIGH);
	digitalWrite(feestlichtPin, HIGH);

	//schakelverlichting potmeters
	schakellicht.setPixelColor(4, 255, 255, 255);
	schakellicht.setPixelColor(5, 255, 255, 255);
	schakellicht.show();

	for(int i=0; i<7; i++){
	    
	    snelheidslicht.setPixelColor(i, 255,255,255);
	}
	snelheidslicht.show();

}

void loop() {

	//----[SCHAKELPANEEL]----//

	//[koplampknop]
	if(digitalRead(koplampKnop) == HIGH){
		//zet koplampen op wit
		for(int i=0; i<7; i++){
		    koplampLinks.setPixelColor(i, 255,255,255);
		    koplampRechts.setPixelColor(i, 255,255,255);
		}
		koplampLinks.show();
		koplampRechts.show();

		schakellicht.setPixelColor(0, 255, 255, 255);
		schakellicht.show();

	}
	else if(digitalRead(koplampKnop) == LOW){
		//zet koplampen op zwart
		for(int i=0; i<7; i++){
		    koplampLinks.setPixelColor(i, 0,0,0);
		    koplampRechts.setPixelColor(i, 0,0,0);
		}
		koplampLinks.show();
		koplampRechts.show();
		schakellicht.setPixelColor(0, 0, 0, 0);
		schakellicht.show();
	}

	//[sireneknop]
	if(digitalRead(sirenestandKnop)== HIGH){

		digitalWrite(zwaailichtPin, LOW);

		//play sirene

		schakellicht.setPixelColor(3, 255, 255, 255);
		schakellicht.show();
	}
	else if(digitalRead(sirenestandKnop)== LOW){

		digitalWrite(zwaailichtPin, HIGH);

		//stop play sirene
		schakellicht.setPixelColor(3, 0, 0, 0);
		schakellicht.show();
	}

	//[knipperlichtknoppen]
	if(digitalRead(knipperLinksKnop) == HIGH){

		//timer knipper bende


		schakellicht.setPixelColor(1, 255, 255, 255);
		schakellicht.show();
	}
	else if(digitalRead(knipperLinksKnop) == LOW){

		knipperlichtLinks.setPixelColor(0,0,0,0);
		knipperlichtLinks.show();

		schakellicht.setPixelColor(1, 0, 0, 0);
		schakellicht.show();
	}

	if(digitalRead(knipperRechtsKnop) == HIGH){

		//timer knipper bende
		schakellicht.setPixelColor(2, 255, 255, 255);
		schakellicht.show();
	}
	else if(digitalRead(knipperRechtsKnop) == LOW){

		knipperlichtRechts.setPixelColor(0,0,0,0);
		knipperlichtRechts.show();

		schakellicht.setPixelColor(2, 0, 0, 0);
		schakellicht.show();
	}

	//[discoknop]
	if(digitalRead(discostandKnop) == HIGH){

		digitalWrite(feestlichtPin, LOW);

		//animeren
		for(int i=0; i<45; i++){
		    cabinelicht.setPixelColor(i,i, i*2, i*3);
		}
		cabinelicht.show();

		schakellicht.setPixelColor(6, 255, 255, 255);
		schakellicht.show();
		
	}
	else if(digitalRead(discostandKnop) == LOW){

		digitalWrite(feestlichtPin, HIGH);

		for(int i=0; i<45; i++){
		    cabinelicht.setPixelColor(i,0, 0, 0);
		}
		cabinelicht.show();

		schakellicht.setPixelColor(6, 0, 0, 0);
		schakellicht.show();
	}

	//[feestlichtKnop]
	if(digitalRead(feestlichtKnop) == HIGH && digitalRead(discostandKnop) == LOW){

		digitalWrite(feestlichtPin, LOW);

		schakellicht.setPixelColor(7, 255, 255, 255);
		schakellicht.show();
	}
	else if(digitalRead(feestlichtKnop) == LOW && digitalRead(discostandKnop) == LOW){

		digitalWrite(feestlichtPin, HIGH);

		schakellicht.setPixelColor(7, 0, 0, 0);
		schakellicht.show();
	}

	//toeterknop
	if(digitalRead(toeterPin) == HIGH){
		
		myDFPlayer.play(1);
	}

	//----[GEARBOX]----//
	
	if 		(digitalRead(gearPin[0]) == HIGH){

		snelheidsmeter.write(0);
	}
	else if (digitalRead(gearPin[1]) == HIGH){
		snelheidsmeter.write(42);
	}
	else if (digitalRead(gearPin[2]) == HIGH){
		snelheidsmeter.write(84);
	}
	else if (digitalRead(gearPin[3]) == HIGH){
		snelheidsmeter.write(126);
	}
	else if (digitalRead(gearPin[4]) == HIGH){
		snelheidsmeter.write(168);
	}
	else if (digitalRead(gearPin[5]) == HIGH){
		snelheidsmeter.write(210);
	}
	else if (digitalRead(gearPin[6]) == HIGH){
		snelheidsmeter.write(252);
	}

	//potmeters als disco uit

}